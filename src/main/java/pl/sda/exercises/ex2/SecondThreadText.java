package pl.sda.exercises.ex2;

public class SecondThreadText implements Runnable{

    private String additionalText;
    int i=0;
    String exclamationMark="!";
    @Override
    public void run(){

        try {
        while (true) {

            System.out.println((i++) + ".Hello World" + exclamationMark + " " + additionalText);
            exclamationMark=exclamationMark+"!";
            Thread.sleep(5000);
        }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    public void setAdditionalText(String additionalText) {
        this.additionalText = additionalText;
    }
}
