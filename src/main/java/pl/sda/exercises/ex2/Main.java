package pl.sda.exercises.ex2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        SecondThreadText secondThreadText=new SecondThreadText();
        Thread thread=new Thread(secondThreadText);
        thread.start();

        Scanner scanner=new Scanner(System.in);
        String line;

        do {
            line=scanner.nextLine();
            secondThreadText.setAdditionalText(line);
        }while (!line.equalsIgnoreCase("quit"));
    }
}
