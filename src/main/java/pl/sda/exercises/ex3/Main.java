package pl.sda.exercises.ex3;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        SecondThreadChecker secondThreadChecker=new SecondThreadChecker();
        Thread thread=new Thread(secondThreadChecker);
        thread.start();

        Scanner scanner=new Scanner(System.in);
        String line;
        do {
            line=scanner.nextLine();
            secondThreadChecker.setDir(line);
        }while (!line.equalsIgnoreCase("quit"));
    }


}

