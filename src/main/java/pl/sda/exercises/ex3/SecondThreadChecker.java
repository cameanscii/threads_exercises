package pl.sda.exercises.ex3;

import java.io.File;

public class SecondThreadChecker implements Runnable{
    private String dir=".";
    int numberOfFiles=0;

    public void setDir(String dir) {
        this.dir = dir;
    }

    @Override
    public void run() {

        try {
            while (true) {
                Thread.sleep(250);
                File folder = new File(dir);


                int files =folder.listFiles().length;

                if (files>numberOfFiles){
                    System.out.println("File added");
                }else if(files<numberOfFiles){
                    System.out.println("File deleted");
                }
                numberOfFiles=files;
            }
            } catch(InterruptedException e){
                e.printStackTrace();
            }


        }


}
